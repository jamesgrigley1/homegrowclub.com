﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Summary description for Utility
/// </summary>
public static class Utility
{
    public static byte[] EncryptPassword(string Password)
    {
        byte[] hashedDataBytes = null;

        UTF8Encoding encoder = new UTF8Encoding();

        MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

        hashedDataBytes = md5Hasher.ComputeHash(encoder.GetBytes(Password));

        return hashedDataBytes;
    }
}