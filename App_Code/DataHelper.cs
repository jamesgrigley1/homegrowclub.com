﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DataHelper
/// </summary>
public static class DataHelper
{
    public static User GetLogin(string UserName, string Password)
    {
        User user = new User();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["db"].ConnectionString);
            cmd.Connection.Open();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "uspGetLogin";
            cmd.Parameters.AddWithValue("@UserName", UserName);
            cmd.Parameters.AddWithValue("@Password", Utility.EncryptPassword(Password));

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    user.UserID = (int)dr["UserID"];
                    user.FirstName = dr["FirstName"].ToString();
                    user.RoleID = (int)dr["RoleID"];
                }
            }

            cmd.Connection.Close();
        }
        return user;
    }

    public static bool ResetPassword(string EmailAddress, string Code, string Password)
    {
        bool boolOK = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["db"].ConnectionString);
            cmd.Connection.Open();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "uspResetPassword";
            cmd.Parameters.AddWithValue("@EmailAddress", EmailAddress);
            cmd.Parameters.AddWithValue("@Code", Code);
            cmd.Parameters.AddWithValue("@Password", Utility.EncryptPassword(Password));

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (int.Parse(dr[0].ToString()) > 0)
                    {
                        boolOK = true;
                    }
                }
            }

            cmd.Connection.Close();
        }
        return boolOK;
    }

    public static bool RegisterUser(string Firstname, string EmailAddress, string Password)
    {
        bool boolOK = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["db"].ConnectionString);
            cmd.Connection.Open();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "uspUpdateUser";
            cmd.Parameters.AddWithValue("@Firstname", EmailAddress);
            cmd.Parameters.AddWithValue("@EmailAddress", EmailAddress);
            cmd.Parameters.AddWithValue("@Password", Utility.EncryptPassword(Password));

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read() && int.Parse(dr[0].ToString()) > 0)
                {
                    boolOK = true;
                }
            }

            cmd.Connection.Close();
        }
        return boolOK;
    }

    public static bool AddForumPost(int UserID, string PostTitle, string PostText)
    {
        bool boolOK = false;
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["db"].ConnectionString);
            cmd.Connection.Open();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "uspUpdateForumThread";
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@ThreadTitle", PostTitle);
            cmd.Parameters.AddWithValue("@PostText", PostText);

            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read() && int.Parse(dr[0].ToString()) > 0)
                {
                    boolOK = true;
                }
            }

            cmd.Connection.Close();
        }
        return boolOK;
    }

    public static DataSet GetForumThreads()
    {
        DataSet ds = new DataSet();
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["db"].ConnectionString);
            cmd.Connection.Open();

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "uspGetForumThreads";

            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
            {
                da.Fill(ds);
            }

            cmd.Connection.Close();
        }
        return ds;
    }
}