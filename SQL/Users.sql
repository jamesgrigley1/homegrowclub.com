Create Table Users
(
	UserID Int Identity(1,1) Primary Key NOT NULL,
	EmailAddress VarChar(200),
	Password Binary(16), --one way encryption
	Firstname nVarChar(250),
	Lastname nVarChar(250),
	UserStatusID Int Default (1) NOT NULL,
	RoleID int,
	LastLogin DateTime Default (GETDATE()) NOT NULL,
	InsertDate DateTime Default (GETDATE()) NOT NULL
)
GO
Create Table UserStatus
(
	UserStatusID Int Identity(0,1) Primary Key NOT NULL,
	UserStatus VarChar(50)
)
GO
Insert Into UserStatus (UserStatus) Values('Deleted')
Insert Into UserStatus (UserStatus) Values('Created')
Insert Into UserStatus (UserStatus) Values('Verified')
Insert Into UserStatus (UserStatus) Values('Suspended')
Go
Create Table UserRoles
(
	RoleID Int Identity(0,1) Primary Key NOT NULL,
	Role VarChar(50)
)
GO
Insert Into UserRoles (Role) Values('User')
Insert Into UserStatus (UserStatus) Values('Admin')
Go
ALTER Procedure uspUpdateUser
	@UserID int = NULL,
	@EmailAddress VarChar(200) = NULL,
	@Password Binary = NULL, --one way encryption
	@Firstname nVarChar(250) = NULL,
	@Lastname nVarChar(250) = NULL,
	@UserStatusID Int = NULL,
	@RoleID Int = NULL
As
	Set NoCount On

	If @UserID IS NULL
		Insert Into Users (
			EmailAddress,
			Password,
			Firstname,
			Lastname
			)
		Values
		(
			@EmailAddress,
			@Password,
			@Firstname,
			@Lastname
			)
	Else
		Update Users
			Set 
				EmailAddress = Case When @EmailAddress IS NULL Then EmailAddress Else @EmailAddress End,
				Password = Case When @Password IS NULL Then Password Else @Password End,
				Firstname = Case When @Firstname IS NULL Then Firstname Else @Firstname End,
				Lastname = Case When @Lastname IS NULL Then Lastname Else @Lastname End,
				UserStatusID = Case When @UserStatusID IS NULL Then UserStatusID Else @UserStatusID End,
				RoleID = Case When @RoleID IS NULL Then RoleID Else @RoleID End
		Where
			UserID=@UserID
GO
CREATE PROCEDURE uspGetLogin
 @UserName VARCHAR(60),
 @Password Binary(16)
AS

	SET NOCOUNT ON

	DECLARE @UserID INT = -1
		
	IF (@username LIKE '%@%.%')
	 BEGIN
	  SELECT
	   @UserID=u.UserID
	  FROM
	   Users u
	  WHERE
	   LOWER(RTRIM(LTRIM(u.EmailAddress)))=LOWER(RTRIM(LTRIM(@Username)))
	   AND u.Password=@Password
	   AND u.UserID = 2

	  IF (@UserID > -1)
	   Begin
		   Update Users Set LastLogin = GETDATE() Where UserID=@UserID

		   Select u.UserID, u.RoleID, u.FirstName From Users u Where u.UserID=@UserID
	   End   
	  ELSE
		-- not found
	   SELECT @UserID=-2
	 END
	ELSE
	 BEGIN
	  -- not an email address
	  SELECT @UserID=-1
	 END







