Create Table ForumGroups
(
	ForumGroupID Int Identity(1,1) Primary Key NOT NULL,
	GroupName VarChar(50),
	GroupDescription VarChar(50),
	GroupStatusID Int Default(1) NOT NULL,
	InsertDate DateTime Default(GETDATE())
)
GO
Create Table ForumGroupStatus
(
	GroupStatusID Int Identity(0,1) Primary Key NOT NULL,
	GroupStatus VarChar(50)
)
GO
Insert Into ForumGroupStatus(GroupStatus) Values('Deleted')
Insert Into ForumGroupStatus(GroupStatus) Values('Active')
Insert Into ForumGroupStatus(GroupStatus) Values('Inactive')
GO
Create Table ForumThreads
(
	ForumThreadID Int Identity(1,1) Primary Key NOT NULL,
	ThreadTitle nVarChar(1000),
	ForumGroupID Int,
	CreatedBy Int,
	InsertDate DateTime Default(GETDATE())

)
--Drop Table ForumPosts
Create Table ForumPosts
(
	ForumPostID Int Identity(1,1) Primary Key NOT NULL,
	PostText nVarChar(2000),
	ForumThreadID Int,
	CreatedBy Int,
	InsertDate DateTime Default(GETDATE())
)