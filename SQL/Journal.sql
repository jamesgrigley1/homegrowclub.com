Create Table Journals
(-- will pull image by JournalId
	JournalID Int Identity(1,1) Primary Key NOT NULL, 
	UserID Int,
	JournalName nVarChar(250),
	JournalStatusID Int,
	InsertDate DateTime GETDATE()
)
Go
Create Table JournalStatus
(
	JournalStatusID Int Identity(0,1) Primary Key NOT NULL,
	JournalStatus VarChar(50)
)
GO
Insert into JournalStatus (JournalStatus) Values('Deleted')
Insert into JournalStatus (JournalStatus) Values('Created')
Insert into JournalStatus (JournalStatus) Values('Active')
Insert into JournalStatus (JournalStatus) Values('Completed')
Insert into JournalStatus (JournalStatus) Values('Closed')
GO
Create Table JournalMetaData
(
	JournalMetaDataID Int Identity(1,1) Primary Key NOT NULL,
	JournalMetaDataTypeID Int,
	JournalMetaDataValue nVarChar(500),
	InsertDate DateTime Default(GETDATE()) NOT NULL
)
GO
Create Table JournalMetaDataTypes
(
	MetaDataTypeID Int Identity(1,1) Primary Key NOT NULL,
	MetaDataType VarChar(50)
)
GO
Insert Into JournalMetaDataTypes (MetaDataType) Values('Strain') -- strain name(s)
Insert Into JournalMetaDataTypes (MetaDataType) Values('Photoperiod') -- specifiy if auto
Insert Into JournalMetaDataTypes (MetaDataType) Values('Grow Medium') 
Insert Into JournalMetaDataTypes (MetaDataType) Values('Lights')
GO
Create Table JournalPosts
(
	JournalPostID Int Identity(1,1) Primary Key NOT NULL,
	JournalID Int,
	Title nVarChar(200),
	PostText nVarChar(8000),
	PostStatusID Int Default (1) NOT NULL,
	InsertDate DateTime Default(Getdate())
)
GO
Create Table JournalPostStatus
(
	PostStatusID Int Identity(0,1) Primary Key NOT NULL,
	PostStatus VarChar(50)
)
GO
Insert into JournalPostStatus (PostStatus) Values('Deleted')
Insert into JournalPostStatus (PostStatus) Values('Created')
Insert into JournalPostStatus (PostStatus) Values('On Hold')
GO
Create Table JournalPostComments
(
	PostCommentID Int Identity(1,1) Primary Key NOT NULL,
	JournalPostID Int,
	PostText nVarChar(1000),
	CommentStatusID Int Default (1) NOT NULL,
	InsertDate DateTime Default(Getdate())
)
GO
Create Table JournalCommentStatus
(
	CommentStatusID Int Identity(0,1) Primary Key NOT NULL,
	CommentStatus VarChar(50)
)
GO
Insert into JournalCommentStatus (CommentStatus) Values('Deleted')
Insert into JournalCommentStatus (CommentStatus) Values('Created')
Insert into JournalCommentStatus (CommentStatus) Values('On Hold')
GO
Create Votes
(
	JournalVoteID Int Identity(1,1) Primary Key NOT NULL,
	VoteTypeID Int,
	JournalItemID Int,
	VoteDirection Bit,
	InsertDate DateTime Default(Getdate())
)
GO
Create Table VoteTypes
(
	VoteTypeID Int Identity(0,1) Primary Key NOT NULL,
	VoteType VarChar(50)
)
GO
Insert into VoteTypes (CommentStatus) Values('Journal')
Insert into VoteTypes (CommentStatus) Values('Journal Post')
Insert into VoteTypes (CommentStatus) Values('Journal Comment')
