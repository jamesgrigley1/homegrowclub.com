﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Interior.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Account_Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="InteriorPlaceHolder" Runat="Server">
    <h1>Register</h1>
    <table id="tblLoginFrame" bordercolor="#999999" cellspacing="0" cellpadding="0" width="500"
        bgcolor="#999999" border="1" runat="server" class="center">
        <tr>
            <td>
                <table cellspacing="1" cellpadding="2" width="100%" bgcolor="#eeeeee" border="0">
                    <tr>
                        <td colspan="2" align="center">
                            
                            <asp:ValidationSummary ID="vsErrors" runat="server" DisplayMode="List" ForeColor="Red"></asp:ValidationSummary>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right"><strong>Firstname:</strong> </td>
                        <td><asp:TextBox ID="txtFirstName" runat="server" /><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFirstName" ErrorMessage="Must enter a firstname"> *</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td align="right"><strong>Email Address:</strong></td>
                        <td><asp:TextBox ID="txtEmail" runat="server" /><asp:RequiredFieldValidator
                                ID="rfvUsername" runat="server" ControlToValidate="txtEmail" ErrorMessage="Must enter an email"> *</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revUsername" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid email address"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <strong>Password:</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" MaxLength="16" Width="176px" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator
                                ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Must enter a password"> *</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <strong>Confirm Password:</strong>
                        </td>
                        <td>
                            <asp:TextBox ID="txtPasswordConfirm" runat="server" MaxLength="16" Width="176px" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPasswordConfirm" ErrorMessage="Must confirm password"> *</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" 
                                ControlToValidate = "txtPassword" 
                                ControlToCompare = "txtPasswordConfirm" 
                                Type = "String" 
                                Operator="Equal" 
                                ErrorMessage="Passwords must match!" 
                                Text ="*"
                                Runat = "Server" /> 
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><asp:Button ID="btnRegister" Text="Register" OnClick="btnRegister_Click" runat="server" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

