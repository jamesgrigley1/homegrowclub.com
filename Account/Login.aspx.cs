﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // we send users to this page to logout, handle that here
        if (Request.QueryString["logout"] != null && Request.QueryString["logout"].ToString() == "true")
        {
            Logout();
        }

        // if user is already authenticated kick them to home page
        if (User.Identity.IsAuthenticated) 
            Response.Redirect("/Default.aspx", true);

        
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        LoginUser();
    }
    protected void btnForgotPassword_Click(object sender, EventArgs e)
    {

    }

    private void LoginUser()
    {
        User user = DataHelper.GetLogin(txtUsername.Text, txtPassword.Text);
        if (user.UserID > 0)
        {
            AuthorizeUser(user);
        }

    }

    private void Logout()
    {
        FormsAuthentication.SignOut();
        Response.Redirect("/default.aspx", true);
    }

    private void AuthorizeUser(User user)
    {
        // Create a new ticket used for authentication
        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
            1, // Ticket version
            user.UserID.ToString(), // Username associated with ticket
            DateTime.Now, // Date/time issued
            DateTime.Now.AddMinutes(500000), // Date/time to expire
            true, // "true" for a persistent user cookie
            user.RoleID.ToString(), // User-data, in this case the roles
            FormsAuthentication.FormsCookiePath);// Path cookie valid for

        // Encrypt the cookie using the machine key for secure transport
        string hash = FormsAuthentication.Encrypt(ticket);
        HttpCookie cookie = new HttpCookie(
            FormsAuthentication.FormsCookieName, // Name of auth cookie
            hash); // Hashed ticket

        // Set the cookie's expiration time to the tickets expiration time
        if (ticket.IsPersistent) cookie.Expires = ticket.Expiration;

        // Add the cookie to the list for outgoing response
        Response.Cookies.Add(cookie);

        Session["FirstName"] = user.FirstName;

        if (Request.QueryString["ReturnURL"] != null)
            Session["ReturnURL"] = Request.QueryString["ReturnURL"].ToString();

        HttpCookie myCookie = new HttpCookie("AuthCookie");
        myCookie["FirstName"] = user.FirstName;
        myCookie["RoleID"] = user.RoleID.ToString();
        myCookie.Expires = DateTime.Now.AddDays(365);
        Response.Cookies.Add(myCookie);

        string returnUrl = Request.QueryString["ReturnUrl"];
        if (returnUrl == null) returnUrl = "/";

        Response.Redirect(returnUrl);
    }
}