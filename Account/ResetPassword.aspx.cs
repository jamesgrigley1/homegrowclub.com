﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_ResetPassword : System.Web.UI.Page
{
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string EmailAddress = "";
        string Code = "";

        if (Request.QueryString["email"] != null)
            EmailAddress = Request.QueryString["email"].ToString();

        if (Request.QueryString["code"] != null)
            Code = Request.QueryString["code"].ToString();

        if (DataHelper.ResetPassword(EmailAddress, txtPasswordNew.Text, Code)){
            // redirect to home page to get picked up by login page
            Response.Redirect("~/Default.aspx"); 
        }
        else
        {
            txtResult.ForeColor = System.Drawing.Color.Red;
            txtResult.Text = "Password reset failed please contact your system administrator.";
        }
    }

}