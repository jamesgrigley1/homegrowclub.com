﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Interior.master" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" Inherits="Account_ResetPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="InteriorPlaceHolder" Runat="Server">
    <p>Reset Password</p>
    <table id="tblLoginFrame" bordercolor="#999999" cellspacing="0" cellpadding="0" width="300"
        bgcolor="#999999" border="1">
        <tr>
            <td>
                <table id="tblLogin" cellspacing="1" cellpadding="2" width="600" bgcolor="#eeeeee" border="0" style="text-align: left;">
                    <tr>

                        <td colspan="3" align="center">
                            <asp:Label ID="txtResult" runat="server" Font-Bold="true">&nbsp;</asp:Label>
                             <asp:ValidationSummary ID="vsErrors" runat="server" Width="472px" Height="34px" ForeColor="Red"></asp:ValidationSummary>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <strong>New Password:</strong>
                        </td>
                        <td style="width: 1px">
                        </td>
                        <td>
                            <asp:TextBox ID="txtPasswordNew" runat="server" MaxLength="16" Width="176px" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator
                                ID="rfvPassword" runat="server" ControlToValidate="txtPasswordNew" ErrorMessage="Must enter a password">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <strong>Confirm Password:</strong>
                        </td>
                        <td style="width: 1px">
                        </td>
                        <td>
                            <asp:TextBox ID="txtPasswordConfirm" runat="server" MaxLength="16" Width="176px" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator
                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPasswordConfirm" ErrorMessage="Must enter a password">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" 
                              ControlToValidate = "txtPasswordNew" 
                              ControlToCompare = "txtPasswordConfirm" 
                              Type = "String" 
                              Operator="Equal" 
                              ErrorMessage="Passwords must match!" 
                              Text ="*"
                              Runat = "Server" /> 
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3">
                            <asp:Button ID="btnLogin" runat="server" Text="Reset Password" OnClick="btnLogin_Click">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

