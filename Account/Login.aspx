﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Interior.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ContentPlaceHolderID="head" ID="whatever" runat="server">
    <script type="text/javascript">
        bShowTabs = false;
        function removeButton(btn) {
            if (!checkSubmitted()) return (false);

            toggleImage(btn);

            return (true);
        }

        function focusBox() {
            var txtUser = getObj('txtUsername');
            var txtPass = getObj('txtPassword');

            if (txtUser.value == '' || txtPass.value != '') {
                txtUser.focus();
            }
            else {
                txtPass.focus();
            }
        }
        focusBox();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="InteriorPlaceHolder" runat="server">

    <asp:Label ID="txtResult" runat="server" Font-Bold="true">&nbsp;</asp:Label>
    <table id="tblLoginFrame" bordercolor="#999999" cellspacing="0" cellpadding="0" width="300"
        bgcolor="#999999" border="1" runat="server" class="center">
        <tr>
            <td>
                <table id="tblLogin" cellspacing="1" cellpadding="2" width="100%" bgcolor="#eeeeee" border="0">
                    <tr>
                        <td colspan="3" align="center">
                            
                            <asp:ValidationSummary ID="vsErrors" runat="server" ForeColor="Red"></asp:ValidationSummary>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 79px" align="right">
                            <strong>Email:</strong>
                        </td>
                        <td style="width: 1px">
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtUsername" runat="server" MaxLength="60" Width="176px"></asp:TextBox><asp:RequiredFieldValidator
                                ID="rfvUsername" runat="server" ControlToValidate="txtUsername" ErrorMessage="Must enter a username">*</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                    ID="revUsername" runat="server" ControlToValidate="txtUsername" ErrorMessage="Invalid username"
                                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 79px" align="right">
                            <strong>Password:</strong>
                        </td>
                        <td style="width: 1px" align="left">
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtPassword" runat="server" MaxLength="16" Width="176px" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator
                                ID="rfvPassword" runat="server" ControlToValidate="txtPassword" ErrorMessage="Must enter a password">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                   
                    <tr>
                        <td align="center" colspan="3">
                            <br />
                            <br />
                            <br />
                            <asp:Button ID="btnLogin" runat="server" Text="Login" Width="200" Height="100" OnClick="btnLogin_Click">
                            </asp:Button>
                             </td>
                    </tr>
                        <td align="center" colspan="3">
                            <br />
                            <br />
                            <br />
                            <br />
                            <asp:CheckBox ID="chkPersistLogin" Text="Stay logged in" Checked="true" runat="server" />
                            

                            <br />
                            <br />
                            <asp:LinkButton ID="btnForgotPassword" Text="[ forgot password ]" CausesValidation="false" OnClick="btnForgotPassword_Click" runat="server" />
                            <br />
                            <br />
                            <br />
                            <asp:HyperLink ID="hl1" NavigateUrl="~/Account/Register.aspx" Text="Register" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content> 