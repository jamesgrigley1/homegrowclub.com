﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        if (DataHelper.RegisterUser(txtFirstName.Text, txtEmail.Text, txtPassword.Text))
        {
            Response.Redirect("~/Account/login.aspx");
        }
    }
}