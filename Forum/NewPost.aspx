﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Interior.master" AutoEventWireup="true" CodeFile="NewPost.aspx.cs" Inherits="Forum_NewPost" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="InteriorPlaceHolder" Runat="Server">
    <table>
        <tr>
            <td>Post Title: </td>
            <td><asp:TextBox ID="txtPostTitle" runat="server" /></td>
        </tr>
        <tr>
            <td>Post Text: </td>
            <td><asp:TextBox ID="txtPostText" TextMode="MultiLine" Rows="5" runat="server" /></td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button ID="btnAddPost" Text="Add Post" OnClick="btnAddPost_Click" runat="server" /></td>
        </tr>
    </table>
</asp:Content>

