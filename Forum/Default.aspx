﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Templates/Interior.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Forum_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="InteriorPlaceHolder" Runat="Server">
   <h1>Forum</h1>
    <asp:Button ID="btnNewPost" Text="New Forum Post" OnClick="btnNewPost_Click" runat="server" />
    <asp:GridView ID="gvMain" AutoGenerateColumns="true" runat="server"></asp:GridView>
</asp:Content>

