﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forum_NewPost : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!User.Identity.IsAuthenticated)
        {
            FormsAuthentication.RedirectToLoginPage();
        }
    }

    protected void btnAddPost_Click(object sender, EventArgs e)
    {
        int UserID = int.Parse(User.Identity.Name);
        if (DataHelper.AddForumPost(UserID, txtPostTitle.Text, txtPostText.Text))
        {
            Response.Redirect("~/Forum/");
        }
    }
}