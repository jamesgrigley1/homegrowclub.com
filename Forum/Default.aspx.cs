﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Forum_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadData();
        }
    }

    private void LoadData()
    {
        gvMain.DataSource = DataHelper.GetForumThreads();
        gvMain.DataBind();
    }

    protected void btnNewPost_Click(object sender, EventArgs e)
    {
        Response.Redirect("NewPost.aspx");
    }
}